import React, { useState } from "react";
import "./Tori.css";

const Tori = () => {
  const [searchText, setSearchText] = useState("Hakusana ja/tai postinumero");
  const [category, setCategory] = useState("Kaikki osastot");
  const [location, setLocation] = useState("Koko Suomi");
  const [saleType, setSaleType] = useState({
    myydaan: false,
    ostetaan: false,
    vuokrataan: false,
    halutaanVuokrata: false,
    annetaan: false,
  });

  const handleSearch = () => {
    
    console.log("Searching with:", searchText, category, location, saleType);
  };

  const handleSaveSearch = () => {
    
    console.log("Search saved!");
  };

  return (
    <div className="tori">
      <div className="txt">
        <input
          type="text"
          className="text"
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
        />
        <select
          className="what"
          value={category}
          onChange={(e) => setCategory(e.target.value)}
        >
          <option value="Kaikki osastot">Kaikki osastot</option>
          
        </select>
        <select
          className="where"
          value={location}
          onChange={(e) => setLocation(e.target.value)}
        >
          <option value="Koko Suomi">Koko Suomi</option>
          
        </select>
      </div>
      <input
        type="checkbox"
        name="how"
        checked={saleType.myydaan}
        onChange={() => setSaleType({ ...saleType, myydaan: !saleType.myydaan })}
      />
      Myydään
      <input
        type="checkbox"
        name="how"
        checked={saleType.ostetaan}
        onChange={() => setSaleType({ ...saleType, ostetaan: !saleType.ostetaan })}
      />
      Ostetaan
      <input
        type="checkbox"
        name="how"
        checked={saleType.vuokrataan}
        onChange={() => setSaleType({ ...saleType, vuokrataan: !saleType.vuokrataan })}
      />
      Vuokrataan
      <input
        type="checkbox"
        name="how"
        checked={saleType.halutaanVuokrata}
        onChange={() => setSaleType({ ...saleType, halutaanVuokrata: !saleType.halutaanVuokrata })}
      />
      Halutaan vuokrata
      <input
        type="checkbox"
        name="how"
        checked={saleType.annetaan}
        onChange={() => setSaleType({ ...saleType, annetaan: !saleType.annetaan })}
      />
      Annetaan
      <p className="hbtn" onClick={handleSaveSearch}>
        Tallenna haku
      </p>
      <button className="btn" onClick={handleSearch}>
        Hae
      </button>
    </div>
  );
};

export default Tori;
